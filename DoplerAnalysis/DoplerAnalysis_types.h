/*
 * DoplerAnalysis_types.h
 *
 * Code generation for model "DoplerAnalysis".
 *
 * Model version              : 1.2
 */

#ifndef RTW_HEADER_DoplerAnalysis_types_h_
#define RTW_HEADER_DoplerAnalysis_types_h_
#include "rtwtypes.h"

/* Model Code Variants */
#ifndef DEFINED_TYPEDEF_FOR_Data_Camera_
#define DEFINED_TYPEDEF_FOR_Data_Camera_

typedef struct {
  int Data_Camera_STR_FIELD_Count;
  int Data_Camera_STR_FIELD_id[50];
  int Data_Camera_STR_FIELD_scanerId[50];
  int Data_Camera_STR_FIELD_detectionStatus[50];
  int Data_Camera_STR_FIELD_type[50];
  int Data_Camera_STR_FIELD_beamIndex[50];
  float Data_Camera_STR_FIELD_existenceTime[50];
  int Data_Camera_STR_FIELD_anchorPoint[50];
  int Data_Camera_STR_FIELD_referenceFrame[50];
  float Data_Camera_STR_FIELD_posXInChosenRef[50];
  float Data_Camera_STR_FIELD_posYInChosenRef[50];
  float Data_Camera_STR_FIELD_posZInChosenRef[50];
  float Data_Camera_STR_FIELD_posHeadingInChosenRef[50];
  float Data_Camera_STR_FIELD_posPitchInChosenRef[50];
  float Data_Camera_STR_FIELD_posRollInChosenRef[50];
  float Data_Camera_STR_FIELD_distanceToCollision[50];
  float Data_Camera_STR_FIELD_azimuthInSensor[50];
  float Data_Camera_STR_FIELD_elevationInSensor[50];
  float Data_Camera_STR_FIELD_azimuthInVehicle[50];
  float Data_Camera_STR_FIELD_elevationInVehicle[50];
  float Data_Camera_STR_FIELD_absoluteSpeedX[50];
  float Data_Camera_STR_FIELD_absoluteSpeedY[50];
  float Data_Camera_STR_FIELD_absoluteSpeedZ[50];
  float Data_Camera_STR_FIELD_absoluteAngularSpeedH[50];
  float Data_Camera_STR_FIELD_absoluteAngularSpeedP[50];
  float Data_Camera_STR_FIELD_absoluteAngularSpeedR[50];
  float Data_Camera_STR_FIELD_relativeSpeedX[50];
  float Data_Camera_STR_FIELD_relativeSpeedY[50];
  float Data_Camera_STR_FIELD_relativeSpeedZ[50];
  float Data_Camera_STR_FIELD_relativeAngularSpeedH[50];
  float Data_Camera_STR_FIELD_relativeAngularSpeedP[50];
  float Data_Camera_STR_FIELD_relativeAngularSpeedR[50];
  float Data_Camera_STR_FIELD_absoluteAccelX[50];
  float Data_Camera_STR_FIELD_absoluteAccelY[50];
  float Data_Camera_STR_FIELD_absoluteAccelZ[50];
  float Data_Camera_STR_FIELD_relativeAccelX[50];
  float Data_Camera_STR_FIELD_relativeAccelY[50];
  float Data_Camera_STR_FIELD_relativeAccelZ[50];
  float Data_Camera_STR_FIELD_length[50];
  float Data_Camera_STR_FIELD_width[50];
  float Data_Camera_STR_FIELD_height[50];
  float Data_Camera_STR_FIELD_visibility[50];
  float Data_Camera_STR_FIELD_visibilityAllBeam[50];
  float Data_Camera_STR_FIELD_rcsValue[50];
} Data_Camera;

#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_Perception_Extract_Ob_T RT_MODEL_Perception_Extract_O_T;

#endif                       /* RTW_HEADER_DoplerAnalysis_types_h_ */
