CC := gcc
# Assembly:
# IdentifyTargetsRearLeft

# Components:
# OcclusionAnalysis
# ExtendedObjectTracker
# PredictedTracks
# FutureCollidingObjectDetector
# DoplerAnalysis
# StaticReflector

CFLAGS := -std=c99 -fPIC
CFLAGS += -I./IdentifyTargetsRearLeft
CFLAGS += -I./IdentifyTargetsRearLeft/linux64/srcGen/src
CFLAGS += -I./OcclusionAnalysis
CFLAGS += -I./ExtendedObjectTracker
CFLAGS += -I./PredictedTracks
CFLAGS += -I./FutureCollidingObjectDetector
CFLAGS += -I./DoplerAnalysis
CFLAGS += -I./StaticReflector

LDFLAGS := -shared -fPIC

C_SOURCES := $(wildcard IdentifyTargetsRearLeft/linux64/srcGen/src/*.c) 
C_SOURCES += $(wildcard OcclusionAnalysis/*.c)
C_SOURCES += $(wildcard ExtendedObjectTracker/*.c)
C_SOURCES += $(wildcard PredictedTracks/*.c)
C_SOURCES += $(wildcard FutureCollidingObjectDetector/*.c)
C_SOURCES += $(wildcard DoplerAnalysis/*.c)
C_SOURCES += $(wildcard StaticReflector/*.c)
OBJECTS = $(C_SOURCES:.c=.o)

TARGET  = IdentifyTargetsRearLeft.so
 
all:$(TARGET)

$(TARGET):$(OBJECTS)
	$(CC) $(LDFLAGS) -o $(TARGET) $(OBJECTS)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf $(TARGET)
