/*
 * StaticReflector.h
 *
 * Code generation for model "StaticReflector".
 *
 */

#ifndef RTW_HEADER_StaticReflector_h_
#define RTW_HEADER_StaticReflector_h_
#include <string.h>
#include <stddef.h>
#ifndef StaticReflector_COMMON_INCLUDES_
#define StaticReflector_COMMON_INCLUDES_

#endif                   /* StaticReflector_COMMON_INCLUDES_ */


/* Shared type includes */


/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#define StaticReflector_M (Perception_Object_Classifica_M)

/* External inputs (root inport signals with default storage) */
typedef struct {
  int x;    /* '<Root>/x' */
  int y;    /* '<Root>/y' */
  int z;    /* '<Root>/z' */
} Vector3;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  float Type_CarIDs[50];              /* '<Root>/Type_CarIDs' */
  float counter_ofCars;               /* '<Root>/counter_ofCars' */
  float Type_BicycleIDs[50];          /* '<Root>/Type_BicycleIDs' */
  float counter_ofBicycles;           /* '<Root>/counter_ofBicycles' */
  float Type_PedestrianIDs[50];       /* '<Root>/Type_PedestrianIDs' */
  float counter_ofPedestrians;        /* '<Root>/counter_ofPedestrians' */
  float Type_TruckIDs[50];            /* '<Root>/Type_TruckIDs' */
  float counter_ofTrucks;             /* '<Root>/counter_ofTrucks' */
  float Type_OtherIDs[50];            /* '<Root>/Type_OtherIDs' */
  float counter_ofOthers;             /* '<Root>/counter_ofOthers' */
} TrackedTargets;

/* Real-time Model Data Structure */
struct tag_RTM_Perception_Object_Cla_T {
  const char *errorStatus;
};

/* External inputs (root inport signals with default storage) */
extern TrackedTargets rpProcessRadarData;

/* External outputs (root outports fed by signals with default storage) */
extern Vector3 ppEllipse;

/* Model entry point functions */
Vector3 getPosAfterSec(float seconds);
Vector3 moveTowards(Vector3 position, Vector3 waypoint, float magnitude);
float distance(Vector3 position, Vector3 waypoint);


/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'StaticReflector'
 * '<S1>'   : 'StaticReflector/Classification'
 */
#endif                      /* RTW_HEADER_StaticReflector_h_ */
