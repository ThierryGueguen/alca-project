/*
 * OcclusionAnalysis.h
 *
 * Code generation for model "OcclusionAnalysis".
 *
 * Model version              : 1.3
 */

#ifndef RTW_HEADER_OcclusionAnalysis_h_
#define RTW_HEADER_OcclusionAnalysis_h_
#include <string.h>
#include <stddef.h>
#ifndef OcclusionAnalysis_COMMON_INCLUDES_
#define OcclusionAnalysis_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* OcclusionAnalysis_COMMON_INCLUDES_ */

#include "OcclusionAnalysis_types.h"

/* Shared type includes */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* External inputs (root inport signals with default storage) */
typedef struct {
  double d_to_col;                     /* '<Root>/d_to_col' */
  double relspeedX;                    /* '<Root>/relspeedX' */
  double VUTspeed;                     /* '<Root>/VUTspeed' */
  double target_detected;              /* '<Root>/target_detected' */
} ExtU_OcclusionAnalysis_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  double TTC;                          /* '<Root>/TTC' */
  double critic;                       /* '<Root>/critic' */
} ExtY_OcclusionAnalysis_T;

/* Real-time Model Data Structure */
struct tag_RTM_OcclusionAnalysis_T {
  const char *errorStatus;
};

/* External inputs (root inport signals with default storage) */
extern ExtU_OcclusionAnalysis_T OcclusionAnalysis_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_OcclusionAnalysis_T OcclusionAnalysis_Y;

/* Model entry point functions */
extern void OcclusionAnalysis_initialize(void);
extern void OcclusionAnalysis_step(void);
extern void OcclusionAnalysis_terminate(void);

/* Real-time Model object */
extern RT_MODEL_OcclusionAnalysis_T *const OcclusionAnalysis_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'OcclusionAnalysis'
 * '<S1>'   : 'OcclusionAnalysis/MATLAB Function1'
 */
#endif                                 /* RTW_HEADER_OcclusionAnalysis_h_ */
