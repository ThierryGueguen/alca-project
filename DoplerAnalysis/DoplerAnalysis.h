/*
 * DoplerAnalysis.h
 *
 * Code generation for model "DoplerAnalysis".
 *
 * Model version              : 1.2
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C source code generated on : Fri Jun  9 15:52:37 2023
 *
 * Target selection: grtfmi.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_DoplerAnalysis_h_
#define RTW_HEADER_DoplerAnalysis_h_
#include <string.h>
#include <stddef.h>
#ifndef DoplerAnalysis_COMMON_INCLUDES_
#define DoplerAnalysis_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                          /* DoplerAnalysis_COMMON_INCLUDES_ */

#include "DoplerAnalysis_types.h"

/* Shared type includes */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* External inputs (root inport signals with default storage) */
typedef struct {
  Data_Camera Data_Sensor;             /* '<Root>/Data_Sensor' */
  float sensor_Id;                    /* '<Root>/sensor_Id' */
  float sensor_Type;                  /* '<Root>/sensor_Type' */
} ExtU_Perception_Extract_Objec_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  float Out1;                         /* '<Root>/Out1' */
  float Out2[50];                     /* '<Root>/Out2' */
  float objectId[50];                 /* '<Root>/objectId' */
  float objectType[50];               /* '<Root>/objectType' */
  float distanceToCollision[50];      /* '<Root>/distanceToCollision' */
  float posXInChosenRef[50];          /* '<Root>/posXInChosenRef' */
  float posYInChosenRef[50];          /* '<Root>/posYInChosenRef' */
  float posZInChosenRef[50];          /* '<Root>/posZInChosenRef' */
  float posHeadingInChosenRef[50];    /* '<Root>/posHeadingInChosenRef' */
  float posPitchInChosenRef[50];      /* '<Root>/posPitchInChosenRef' */
  float posRollInChosenRef[50];       /* '<Root>/posRollInChosenRef' */
  float relativeSpeedX[50];           /* '<Root>/relativeSpeedX' */
  float relativeSpeedY[50];           /* '<Root>/relativeSpeedY' */
  float relativeSpeedZ[50];           /* '<Root>/relativeSpeedZ' */
  float relativeAccelX[50];           /* '<Root>/relativeAccelX' */
  float relativeAccelY[50];           /* '<Root>/relativeAccelY' */
  float relativeAccelZ[50];           /* '<Root>/relativeAccelZ' */
  float length[50];                   /* '<Root>/length' */
  float width[50];                    /* '<Root>/width' */
  float height[50];                   /* '<Root>/height' */
} ExtY_Perception_Extract_Objec_T;

/* Real-time Model Data Structure */
struct tag_RTM_Perception_Extract_Ob_T {
  const char *errorStatus;
};

/* External inputs (root inport signals with default storage) */
extern ExtU_Perception_Extract_Objec_T DoplerAnalysis_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_Perception_Extract_Objec_T DoplerAnalysis_Y;

/* Model entry point functions */
extern void DoplerAnalysis_initialize(void);
extern void DoplerAnalysis_step(void);
extern void DoplerAnalysis_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Perception_Extract_O_T *const DoplerAnalysis_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'DoplerAnalysis'
 * '<S1>'   : 'DoplerAnalysis/Extract Objects'
 */
#endif                             /* RTW_HEADER_DoplerAnalysis_h_ */
