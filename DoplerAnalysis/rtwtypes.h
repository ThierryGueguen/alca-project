/*
 * rtwtypes.h
 *
 * Code generation for model "Dopler_Analysis".
 *
 * Model version              : 1.2
 */

#ifndef RTWTYPES_H
#define RTWTYPES_H

#ifndef POINTER_T
#define POINTER_T

typedef void * pointer_T;

#endif

/* Logical type definitions */
#if (!defined(__cplusplus))
#ifndef false
#define false                          (0U)
#endif

#ifndef true
#define true                           (1U)
#endif
#endif
#endif                                 /* RTWTYPES_H */
