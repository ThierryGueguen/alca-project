/*
 * ExtendedObjectTracker_types.h
 *
 * Code generation for model "ExtendedObjectTracker".
 *
 * Model version              : 1.3
 */

#ifndef RTW_HEADER_ExtendedObjectTracker_types_h_
#define RTW_HEADER_ExtendedObjectTracker_types_h_
#include "rtwtypes.h"

/* Model Code Variants */
#ifndef DEFINED_TYPEDEF_FOR_DetectedObjects_
#define DEFINED_TYPEDEF_FOR_DetectedObjects_

typedef struct {
  float counter_ofVisible;
  float visibilityAllBeam[50];
  float objectId[50];
  float objectType[50];
  float distanceToCollision[50];
  float posXInChosenRef[50];
  float posYInChosenRef[50];
  float posZInChosenRef[50];
  float posHeadingInChosenRef[50];
  float posPitchInChosenRef[50];
  float posRollInChosenRef[50];
  float relativeSpeedX[50];
  float relativeSpeedY[50];
  float relativeSpeedZ[50];
  float relativeAccelX[50];
  float relativeAccelY[50];
  float relativeAccelZ[50];
  float length[50];
  float width[50];
  float height[50];
} DetectedObjects;

#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_Perception_Object_Cla_T RT_MODEL_Perception_Object_Cl_T;

#endif                /* RTW_HEADER_ExtendedObjectTracker_types_h_ */
