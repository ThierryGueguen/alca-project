/*
 * OcclusionAnalysis_types.h
 *
 * Code generation for model "OcclusionAnalysis".
 *
 * Model version              : 1.3
 */

#ifndef RTW_HEADER_OcclusionAnalysis_types_h_
#define RTW_HEADER_OcclusionAnalysis_types_h_

/* Model Code Variants */

/* Forward declaration for rtModel */
typedef struct tag_RTM_OcclusionAnalysis_T RT_MODEL_OcclusionAnalysis_T;

#endif                                 /* RTW_HEADER_OcclusionAnalysis_types_h_ */
